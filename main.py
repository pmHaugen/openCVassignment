from datetime import timedelta
import numpy as np
import cv2 as cv
import time

cap = cv.VideoCapture(0)

if not cap.isOpened():
    print("Cannot open camera")
    exit()

fourcc = cv.VideoWriter_fourcc(*'XVID')
out = cv.VideoWriter('output.avi', fourcc, 20.0, (640, 480))
start = time.time()
lagimagetimerstart = time.time()
text = "dne"
font = cv.FONT_HERSHEY_SIMPLEX

textpos = 0
while True:
    ret, frame = cap.read()
    end = time.time()
    lagimagetimerend = time.time()
    text = "Normal"
    textcolor = [0,0,0]
    if not ret:
        print("Can't receieve frame (stream end?). Exiting ...")
    if end-start > 5 and end-start < 15:
        frame = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
        text = "Grayscale"
        textcolor = [0,0,0]
    if end-start > 15 and end-start < 25:
        text = "Weighted images with lag"
        if lagimagetimerend-lagimagetimerstart > 0.5:
            image2 = frame
            lagimagetimerstart = time.time()
        image = frame
        frame = cv.addWeighted(image,0.6,image2,1,0)
    if end-start > 25 and end-start < 35:
        text= "masking"
        textcolor = [255,255,255]
        hsv = cv.cvtColor(frame, cv.COLOR_BGR2HSV)
        lower_blue = np.array([20, 10, 20])
        upper_blue = np. array([130,240,130])
        mask = cv.inRange(hsv, lower_blue, upper_blue)
        res = cv.bitwise_and(frame, frame, mask= mask)
        frame = res
        #cv.imshow('mask', mask)
        #cv.imshow('res', res)
    if end-start > 35 and end-start < 45:
        text = "cut"
        topleft = frame[0:240, 0:320]
        bottomleft = frame[240:480, 0:320]
        topright = frame[0:240, 320:640]
        bottomright = frame[240:480, 320:640]
        ret, tempframe = cap.read()
        
        tempframe[0:240, 0:320] = bottomleft
        tempframe[240:480, 0:320] = bottomright
        tempframe[0:240, 320:640] = topleft
        tempframe[240:480, 320:640] = topright

        frame = tempframe
        
    if end-start > 45 and end-start < 55:
        text = "Merged frames"
        topleft = frame[0:240, 0:320]
        bottomleft = frame[240:480, 0:320]
        topright = frame[0:240, 320:640]
        bottomright = frame[240:480, 320:640]
        ret, tempframe = cap.read()
        tempframe[0:240, 0:320] = bottomright
        tempframe[240:480, 0:320] = bottomleft
        tempframe[0:240, 320:640] = topright
        tempframe[240:480, 320:640] = topleft


        frame[0:480, 0:640] = frame+tempframe
    if end-start > 55:

        print(end-start)
        break
        #start = time.time()
        
    if textpos > 600:
        textpos = -300
    else:
         textpos += 10
    frame = cv.putText(frame, text, (textpos,400), font, 1, (textcolor),2,cv.LINE_AA)
       
    out.write(frame)
    cv.imshow('Amazing Webcam app', frame)
    if cv.waitKey(1) == ord('q'):
        break

cap.release()
out.release()
cv.destroyAllWindows()